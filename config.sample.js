/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "config" }] */

// config object with list of available words for this exercise
// in the integrated version this will be generated dynamically by the backend
const config = {
  words: [
    { full: 'Ernte', segments: ['E', 'r', 'n', 't', 'e'] },
    { full: 'Arbeit', segments: ['A', 'r', 'b', 'e', 'i', 't'] },
    { full: 'schwer', segments: ['sch', 'w', 'e', 'r'] },
    { full: 'Tage', segments: ['T', 'a', 'g', 'e'] },
    { full: 'Woche', segments: ['W', 'o', 'c', 'h', 'e'] },
    { full: 'Uhr', segments: ['U', 'h', 'r'] },
    { full: 'Früh', segments: ['F', 'r', 'ü', 'h'] },
    { full: 'Euro', segments: ['E', 'u', 'r', 'o'] },
    { full: 'Stunde', segments: ['S', 't', 'u', 'n', 'd', 'e'] },
    { full: 'Lohn', segments: ['L', 'o', 'h', 'n'] },
    { full: 'Rechte', segments: ['R', 'e', 'c', 'h', 't', 'e'] },
    { full: 'Chef', segments: ['C', 'h', 'e', 'f'] },
    { full: 'Gericht', segments: ['G', 'e', 'r', 'i', 'c', 'h', 't'] },
  ],
  // the title / header of the exercise
  title: 'Buchstaben-Puzzle',
  // a sentence or short paragraph to describe the exercise
  description: 'Hör dir das Wort an und schiebe die Elemente in die leeren Kästchen.',
  // link to the next exercise / page
  next: '#this-link-does-not-lead-anywhere-yet',
  // labels that are shown as links to the next word / exercise (or page)
  nextWordLabel: 'Nächstes Wort',
  nextLabel: 'Nächste Übung',
  // directory where audio files for words and segments are stored
  audioSource: '../audio/',
}
