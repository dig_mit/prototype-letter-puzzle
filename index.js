/*
// the following should be sourced from config.js before the index.js is loaded
const config = {
  words: [
    { full: "gearbeitet", segments: [ "ge", "ar", "bei", "tet" ] },
    { full: "Erntehelfer", segments: [ "Ern", "te", "hel", "fer" ] },
    // and so on
  ],
  // link to the next exercise
  next: "#this-link-does-not-lead-anywhere-yet",
  // directory where audio files for words and segments are stored
  audioSource: "../audio/"
}
 */

// set config and $ as global vars for the linter
/* global config */
/* global $ */

// the following two variables are to be compatible with the final WP plugin implementation
const DigMitConfig = config
const digMitExerciseID = 1

class LetterPuzzle {
  constructor (id, config) {
    this.config = config
    this.id = id
    this.$container = $('#digmit-exercise-container-' + id)
  }

  init () {
    this.initialiseContainer()
    $('title').text(this.config.title)
    $('.header h1').text(this.config.title)
    $('.description').text(this.config.description)
    this.initialiseSegments()
    this.initialiseTargets()
    $('#next-word-button').on('click', { self: this }, this.prepareNextWord)
    $('#next-word-button span').text(this.config.nextWordLabel)
    $('#next-exercise-button span').text(this.config.nextLabel)
    $('#next-exercise-button').on('click', () => {
      window.location = this.config.next
    })
  }

  initialiseContainer () {
    $('<div class="header"><h1></h1></div>').appendTo(this.$container)
    $('<div class="description"></div>').appendTo(this.$container)
    $('<div class="target-field"></div>').appendTo(this.$container)
    $('<div class="source-field"></div>').appendTo(this.$container)

    const elSuccessMessage = `
      <div class="success-message" style="display: none">
        <p>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          Super!
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
        </p>
        <div id="next-exercise-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $(elSuccessMessage).appendTo(this.$container)

    const elNextWordMessage = `
      <div class="next-word-message" style="display: none">
        <p>
          <i class="fa fa-star fa-spin"></i>
          Super!
          <i class="fa fa-star fa-spin"></i>
        </p>
        <div id="next-word-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $(elNextWordMessage).appendTo(this.$container)
  }

  // take the next word from config.words array, create elements for the segments
  // and place them in the source area
  initialiseSegments () {
    const config = this.config

    // check if this is the frist word in the exercise and create counter
    // otherwise increase counter for the next word
    if (config.counter === undefined) {
      config.counter = 0
    } else {
      config.counter++
    }

    // shuffle the segments and make sure the first one is not the first of the word
    let unshuffled = config.words[config.counter].segments
    // make a copy to not manipulate the original array
    unshuffled = unshuffled.slice(0, unshuffled.length)
    const shuffled = []
    while (unshuffled.length > 0) {
      const i = Math.floor(Math.random() * unshuffled.length)
      // an index of 0 is only ok, if we're not filling the first segment
      if (shuffled.length === 0 && i === 0) {
        continue
      }
      shuffled.push(unshuffled.splice(i, 1)[0])
    }

    // first add a non-draggable audio button for the whole word
    let $el = this.createFullWord(this, config.words[config.counter].full)
    $el.insertAfter($('.description'))

    // now we can add all words as draggables to the source field
    for (let i in shuffled) {
      const el = '<div class="audio-button" id="segment-' + i + '">' + shuffled[i] + '<br><i class="fa fa-volume-up"></i></div>'
      $el = $(el)
      $el.appendTo($('.source-field'))
      $el.draggable({ revert: true })
      $el.on('click', { label: shuffled[i], source: config.audioSource }, this.playAudio)
      i++
    }

    // allow audio buttons to be moved back to the source field
    $('.source-field').droppable({
      accept: '.audio-button',
      hoverClass: 'drop-hover',
    }).on('drop', { self: this }, this.processDrop)
  }

  // initialise all target fields as droppables
  initialiseTargets () {
    const config = this.config
    const segments = config.words[config.counter].segments
    for (let i = 0; i < segments.length; i++) {
      const el = '<div id="target-' + i + '"></div>'
      const $el = $(el)
      $el.appendTo($('.target-field'))
      $el.droppable({
        accept: '.audio-button',
        hoverClass: 'drop-hover',
      }).on('drop', { self: this }, this.processDrop)
    }
  }

  // play an audio file associated with a word
  playAudio (event) {
    const source = event.data.source
    const label = event.data.label
    const audio = new Audio(source + label + '.mp3')
    audio.play()
  }

  // creates and returns a clickable and non-draggable button with a text
  // that is played back when the user clicks on it
  createFullWord (self, text) {
    const el = '<div class="full-word"><i class="fa fa-volume-up"></i></div>'
    const $el = $(el)
    $el.on('click', { label: text, source: self.config.audioSource }, self.playAudio)
    return $el
  }

  // when a segment was dropped into one of the targets, we'll process it here:
  // 1. move the element to the target area
  // 2. check if the answer is correct and add corresponding class
  processDrop (event, ui) {
    const self = event.data.self
    const config = self.config
    const $target = $(event.target)
    const $segment = ui.draggable
    const word = config.words[config.counter]

    // move the draggable to the target area
    $segment.appendTo($target)

    // if the draggable was moved back to the source field we just have
    // to remove the checkmark and are done here
    if ($target.hasClass('source-field')) {
      // first we remove the <i> with the checkmark
      $segment.children().last().remove()
      // then the <br> that is still there
      $segment.children().last().remove()
      return
    }

    // get the numeric place of the target segment (the id part after "target-")
    const i = parseInt($target.attr('id').substr(7))
    // if the string already as a checkmark or cross, remove it first
    let html = $segment.html()
    if (html.endsWith('<br><i class="fa fa-check correct"></i>')) {
      html = html.substr(0, html.length - '<br><i class="fa fa-check correct"></i>'.length)
    } else if (html.endsWith('<br><i class="fa fa-times incorrect"></i>')) {
      html = html.substr(0, html.length - '<br><i class="fa fa-times incorrect"></i>'.length)
    }
    // now add a checkmark of cross depending on whether it is placed correctly
    if ($segment.text() === word.segments[i]) {
      html += '<br><i class="fa fa-check correct"></i>'
    } else {
      html += '<br><i class="fa fa-times incorrect"></i>'
    }
    // set the new html content
    $segment.html(html)

    // check if all items are already placed correctly and set visibility for
    // the next word or success message accordingly
    if (self.allCorrect(word)) {
      // remove the draggable from the audio buttons and add exercise-solved class
      $('.audio-button').draggable('destroy')
      $('.audio-button').addClass('exercise-solved')
      // remove the source field and move the audio button for the whole word into the target area
      $('.source-field').remove()
      $('.full-word').appendTo($('.target-field'))
      // now check if this was already the last word in the exercise
      if (config.counter + 1 >= config.words.length) {
        $('.success-message').css('display', 'block')
        $('.success-message a').attr('href', config.next)
      } else {
        $('.next-word-message').css('display', 'block')
      }
    } else {
      $('.success-message').css('display', 'none')
    }
  }

  // check if all fields have been placed in their final position
  // and return true or false accordingly
  allCorrect (word) {
    // if there are still items in the source field, we are not done
    if ($('.source-field').first().children().length !== 0) {
      return false
    }
    // now check for each segment, if the target is placed correctly
    let correct = true
    for (const i in word.segments) {
      if ($('#target-' + i).children().first().text() !== word.segments[i]) {
        correct = false
        break
      }
    }
    if (correct) return true
    return false
  }

  // this is called after a word was solved to prepare for the next word in the
  // exercise. we have to restore source and target fields accordingly
  prepareNextWord (event) {
    const self = event.data.self
    // make the next word messsage invisible again
    $('.next-word-message').css('display', 'none')
    // let's also remove all elements from the target field
    $('.target-field').children().remove()
    // and insert a new source field
    const el = '<div class="source-field"></div>'
    $(el).insertAfter($('.target-field'))
    // now lets re-initialise
    self.initialiseSegments()
    self.initialiseTargets()
  }
}

// start initialisation as soon as the document is ready
$('document').ready(function () {
  const exercise = new LetterPuzzle(digMitExerciseID, DigMitConfig)
  exercise.init()
})
